# Proyecto Iniciación GraphQL

- Curso para principiantes de GraphQL

## Tecnologías:

- Node/express 4.17.1
- mongo
- graphql 15.5.1
- graphq-tools 8.1.0

## Contenido:

- CRUD de personas, cursos
  - Leer todos los cursos y personas
  - Leer datos de solo un curso o persona
  - Actualizar curso o persona
  - Añadir personas a un curso - Eliminar personas o cursos

## Conceptos vistos:

- Crear esquema
- Inicializar un proyecto con node y GraphQL
- Queries y mutations
- Resolvers
- Nested types
- Fragments y aliases

## Comandos:

### Lanzar aplicación en modo producción

#### - Linux

`npm run startOnLinux`

#### - Windows

`npm start`

### Lanzar aplicación en modo desarrollo

`npm run dev`

### Comprobar lint

`npm run lint`

### Corregir errores de lint

`npm run lint-fix`
